* Hello, Universe! - In C
** The command line and compiler
** Types and variables
** Functions
** Conditionals
** Loops
** Memory I - Stack and heap
** Memory II - Pointers
** Memory III - Allocation
** Data structures I - Arrays
** Data structures II - Structs
** Preprocessor commands
** Libraries
